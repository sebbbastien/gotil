package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	green  = "\033[0;32m%s\033[0m"
	yellow = "\033[0;33m%s\033[0m"
	red    = "\033[1;31m%s\033[0m"
)

var osExit = os.Exit

/** defineTargetFromTime ...
 *
 *  @param now have to be pass as argument to be testable
 */
func defineTargetFromTime(now time.Time, h, m int) (t time.Time) {
	t = time.Date(now.Year(), now.Month(), now.Day(), h, m, 00, 00, now.Location())

	if t.Sub(now) < 0 {
		t = t.Add(time.Hour * 24)
	}

	return t
}

func parseArgs(arg string) (h, m int, err error) {
	var re = regexp.MustCompile(`(?m)^(?P<h>[0-9]|0[0-9]|1[0-9]|2[0-3]):(?P<m>[0-5][0-9])$`)
	match := re.FindAllStringSubmatch(arg, -1)

	if match == nil {
		return -1, -1, fmt.Errorf("%s is not a valid target", arg)
	}

	h, _ = strconv.Atoi(match[0][1])
	m, _ = strconv.Atoi(match[0][2])

	return h, m, nil
}

func colorOutput(t time.Duration) (res string) {
	res = t.String()
	if t > time.Hour {
		return fmt.Sprintf(green, res)
	}
	if t > time.Minute*15 {
		return fmt.Sprintf(yellow, res)
	}
	return fmt.Sprintf(red, res)
}

func sleepUntil(target time.Time) {
	var left time.Duration

	for {
		left = time.Until(target).Truncate(time.Second)

		if left <= time.Second {
			// Last chance to sync with target
			time.Sleep(time.Until(target))

			fmt.Printf("\rEnd (%s)\n", time.Now().Format("15:04:05.000"))
			return
		}

		os.Stdout.Write([]byte("\r" + strings.Repeat(" ", 10)))
		os.Stdout.Write([]byte("\r" + colorOutput(left)))

		time.Sleep(1 * time.Second)
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Takes one and only one argument.")
		osExit(1)
		return
	}

	var target time.Time

	if os.Args[1] == "now" {
		osExit(0)
	} else if strings.Contains(os.Args[1], ":") {
		h, m, err := parseArgs(os.Args[1])
		if err != nil {
			osExit(2)
			return
		}
		target = defineTargetFromTime(time.Now(), h, m)
	} else {
		d, err := time.ParseDuration(os.Args[1])
		if err != nil {
			osExit(2)
			return
		}
		target = time.Now().Add(d)
	}

	fmt.Printf("Target : %s (%vs)\n",
		target.Format("15:04"),
		time.Until(target).Truncate(time.Second).Seconds())

	sleepUntil(target)
	osExit(0)
}
