package main

import (
	"fmt"
	"os"
	"reflect"
	"testing"
	"time"
)

func Test_parseArgs(t *testing.T) {
	type args struct {
		arg string
	}
	tests := []struct {
		name    string
		args    args
		wantH   int
		wantM   int
		wantErr bool
	}{
		//! Add test cases.
		{name: "Basic case", args: args{"1:04"}, wantH: 1, wantM: 4, wantErr: false},
		{name: "Basic case", args: args{"1:34"}, wantH: 1, wantM: 34, wantErr: false},
		{name: "Basic case", args: args{"12:03"}, wantH: 12, wantM: 3, wantErr: false},
		{name: "Bad hh", args: args{"32:34"}, wantH: -1, wantM: -1, wantErr: true},
		{name: "Bad mm", args: args{"12:99"}, wantH: -1, wantM: -1, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotH, gotM, err := parseArgs(tt.args.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseArgs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotH != tt.wantH {
				t.Errorf("parseArgs() gotH = %v, want %v", gotH, tt.wantH)
			}
			if gotM != tt.wantM {
				t.Errorf("parseArgs() gotM = %v, want %v", gotM, tt.wantM)
			}
		})
	}
}

func Test_colorOutput(t *testing.T) {
	type args struct {
		t time.Duration
	}
	tests := []struct {
		name    string
		args    args
		wantRes string
	}{
		//! Add test cases.
		{name: "Green output",
			args:    args{time.Hour * 2},
			wantRes: "\033[0;32m2h0m0s\033[0m",
		},
		{name: "Yellow output",
			args:    args{time.Minute * 30},
			wantRes: "\033[0;33m30m0s\033[0m",
		},
		{name: "Red and bold output",
			args:    args{time.Minute*3 + time.Second*45},
			wantRes: "\033[1;31m3m45s\033[0m",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRes := colorOutput(tt.args.t); gotRes != tt.wantRes {
				t.Errorf("colorOutput() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}

func Test_defineTargetFromTime(t *testing.T) {
	type args struct {
		now time.Time
		h   int
		m   int
	}
	tests := []struct {
		name  string
		args  args
		wantT time.Time
	}{
		//! Add test cases.
		{name: "Same day",
			args:  args{time.Date(2020, 01, 20, 20, 3, 56, 45, time.UTC), 23, 59},
			wantT: time.Date(2020, 01, 20, 23, 59, 00, 00, time.UTC),
		},
		{name: "Next day",
			args:  args{time.Date(2020, 01, 20, 20, 3, 56, 45, time.UTC), 19, 9},
			wantT: time.Date(2020, 01, 21, 19, 9, 00, 00, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotT := defineTargetFromTime(tt.args.now, tt.args.h, tt.args.m); !reflect.DeepEqual(gotT, tt.wantT) {
				t.Errorf("defineTarget() = %v, want %v", gotT, tt.wantT)
			}
		})
	}
}

func Test_sleepUntil(t *testing.T) {
	type args struct {
		delay int
	}
	tests := []struct {
		name string
		args args
	}{
		//! Add test cases.
		{name: "Sleep 5 seconds",
			args: args{delay: 5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			target := time.Now().Add(time.Second * 5)
			stdout := os.Stdout
			os.Stdout = nil
			sleepUntil(target)
			os.Stdout = stdout
		})
	}
}

func Test_main(t *testing.T) {
	type args struct {
		args []string
	}
	now := time.Now()
	tests := []struct {
		name     string
		args     args
		wantExit int
	}{
		//! Add test cases.
		{name: "Without aruments",
			args:     args{args: []string{}},
			wantExit: 1,
		},
		{name: "With to many arguments",
			args:     args{args: []string{"aa", "bb"}},
			wantExit: 1,
		},
		{name: "Bad arguments (67:89)",
			args:     args{args: []string{"67:89"}},
			wantExit: 2,
		},
		{name: "Bad arguments (12)",
			args:     args{args: []string{"12"}},
			wantExit: 2,
		},
		{name: "Bad arguments (foo)",
			args:     args{args: []string{"foo"}},
			wantExit: 2,
		},
		{name: "Bad arguments (foo:bar)",
			args:     args{args: []string{"foo:bar"}},
			wantExit: 2,
		},
		{name: "Sleep 1 minute",
			args:     args{args: []string{fmt.Sprintf("%d:%d", now.Hour(), now.Minute()+1)}},
			wantExit: 0,
		},
		{name: "Now",
			args:     args{args: []string{"now"}},
			wantExit: 0,
		},
		{name: "Sleep 20 seconds",
			args:     args{args: []string{"20s"}},
			wantExit: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Mock osExit|os.Exit()
			oldOsExit := osExit
			defer func() { osExit = oldOsExit }()

			var gotExit int
			myExit := func(code int) {
				gotExit = code
			}

			osExit = myExit

			os.Args = append([]string{"cmd"}, tt.args.args...)

			stdout := os.Stdout
			os.Stdout = nil

			main()

			os.Stdout = stdout

			if gotExit != tt.wantExit {
				t.Errorf("Expected exit code: %d, got: %d", tt.wantExit, gotExit)
			}
		})

	}
}
